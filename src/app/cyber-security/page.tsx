// components
import SecurityTemplate from '@/modules/cyberSecurity/components/securityTemplate/securityTemplate';
// styles
import './cyberSecurityPage.scss';

const CyberSecurityPage = (): JSX.Element => {
  // Example text
  const examples = [
    "<img onerror=alert(document.cookie); src='invalid.url.com'>",
    "<img src='http://url.to.file.which/not.exist' onerror=alert(document.cookie);>",
    "<b onmouseover=alert('Wufff!');>click me!</b>",
  ];

  return (
    <main className='p-cyber-security container'>
      <section className='p-cyber-security--container'>
        <h1>Formularios de test</h1>
        <span className='p-cyber-security__line' />

        <hr className='full-w' />
        <section>
          <ul>
            {examples.map((exa) => (
              <li key={exa}>{exa}</li>
            ))}
          </ul>
        </section>
        <hr className='full-w' />

        <SecurityTemplate />
      </section>
    </main>
  );
};

export default CyberSecurityPage;
