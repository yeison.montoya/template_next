'use client';
import { useState } from 'react';
// auth
import { signIn, useSession } from 'next-auth/react';
// Router
import { useSearchParams, useRouter } from 'next/navigation';
// components
import { Button, Input, Loading } from '@/components/index';

enum loginStates {
  password = 'password',
  isLoading = 'isLoading',
}

const LoginPage = () => {
  // auth
  const { data: session, status } = useSession();

  // router
  const router = useRouter();
  const searchParams = useSearchParams();

  // state
  const defaultState = {
    [loginStates.password]: '',
    [loginStates.isLoading]: false,
  };

  const [loginState, setLoginState] = useState(defaultState);

  // action states
  const handlerLoginState = ({
    key,
    value,
  }: {
    key: loginStates;
    value: string | boolean;
  }) => {
    setLoginState((prevSate) => ({
      ...prevSate,
      [key]: value,
    }));
  };

  const resetLoginState = () => setLoginState(defaultState);

  // data
  const callbackUrl = searchParams.get('callbackUrl') ?? '/';

  // submit
  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      handlerLoginState({ key: loginStates.isLoading, value: true });

      const res = await signIn('credentials', {
        redirect: false,
        password: loginState.password,
      });

      if (!res?.ok || res.error) {
        return resetLoginState();
      }

      router.refresh();
      router.push(callbackUrl);
    } catch (error: any) {
      handlerLoginState({ key: loginStates.isLoading, value: false });
    }
  };

  if (status === 'loading')
    return (
      <main className='container'>
        <Loading />
      </main>
    );

  if (session?.authenticated) return router.push(callbackUrl);

  return (
    <main className='container'>
      <h1>Login Page</h1>
      <form onSubmit={(e) => onSubmit(e)}>
        <Input
          label='Password'
          type='password'
          id='Password'
          name='Password'
          onChange={(e) =>
            handlerLoginState({
              key: loginStates.password,
              value: e.target.value,
            })
          }
        />

        {loginState.isLoading ? (
          <Loading />
        ) : (
          <Button
            variant='primary'
            type='submit'
            disabled={!loginState.password}
          >
            Sign in with password
          </Button>
        )}
      </form>
    </main>
  );
};

export default LoginPage;
