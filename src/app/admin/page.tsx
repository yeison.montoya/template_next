const AdminPage = () => {
  return (
    <main className='container'>
      <h1>Admin page</h1>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Atque facere
        veritatis tenetur explicabo minus veniam eligendi officia soluta, dicta
        sint inventore a. Molestiae commodi facilis, ipsum nesciunt cumque
        consequatur ipsa?
      </p>
    </main>
  );
};

export default AdminPage;
