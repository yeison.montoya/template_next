// auth
import { getServerSession } from 'next-auth';
// styles
import './homePage.scss';

const HomePage = async () => {
  const session = await getServerSession();

  const name = session?.user?.name;

  return (
    <main className='p-home-page container'>
      <h1>Home Page</h1>
      <h2>{name}</h2>

      <p>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Amet earum
        eaque veniam in repudiandae, voluptatem ab eius fuga, veritatis deleniti
        voluptatibus nisi dignissimos saepe? Velit maiores molestias quae quo
        id.
      </p>
      <br />
      <p>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Amet earum
        eaque veniam in repudiandae, voluptatem ab eius fuga, veritatis deleniti
        voluptatibus nisi dignissimos saepe? Velit maiores molestias quae quo
        id.
      </p>
    </main>
  );
};

export default HomePage;
