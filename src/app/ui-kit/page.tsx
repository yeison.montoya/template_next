// components
import { Button, Input, Select, Collapse } from '@/components/';
import ModalExample from '@/modules/uiKit/components/modalExample/modalExample';
import Icons from '@assets/icons/icons';
// Icons
import { Add, Upload } from '@icons/index';
// styles
import './uiKitPage.scss';

const selectOption = [
  { value: 'text 1', label: 'Text 1' },
  { value: 'text 2', label: 'Text 2' },
  { value: 'text 3', label: 'Text 3' },
  { value: 'text 4', label: 'Text larger' },
];

const UIKitPage = () => {
  return (
    <main className='container'>
      <h1>UIKit Examples</h1>
      <h2>Buttons</h2>
      <hr className='full-w' />
      <section className='p-ui-kit--section p-ui-kit--section__body'>
        <Button>Default</Button>
        <Button>
          Default Icon <Upload />
        </Button>

        <Button variant='primary'>Primary</Button>
        <Button variant='primary'>
          <Upload />
          Primary Icon
        </Button>
        <Button variant='primary' large='small'>
          Primary small
        </Button>

        <Button variant='secondary'>Secondary</Button>
        <Button variant='secondary'>
          <Upload />
          Secondary Icon
        </Button>
        <Button variant='secondary' large='small'>
          Secondary small
        </Button>

        <Button variant='white'>White</Button>
        <Button variant='white'>
          <Upload />
          White Icon
        </Button>
        <Button variant='white' large='small'>
          White small
        </Button>

        <Button variant='primary' rounded='icon'>
          <Add />
        </Button>
        <Button variant='primary' rounded='icon' large='small'>
          <Add />
        </Button>

        <Button variant='secondary' rounded='icon'>
          <Add />
        </Button>
        <Button variant='secondary' rounded='icon' large='small'>
          <Add />
        </Button>
      </section>
      <h2>Text definitions</h2>
      <hr className='full-w' />
      <section className='p-ui-kit--section__body'>
        <h1>Title</h1>
        <h1 className='bold'>Title bold</h1>
        <h2>H2 title</h2>
        <h2 className='bold'>H2 title bold</h2>
        <h3>H3 title</h3>
        <h3 className='bold'>H3 title bold</h3>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Reiciendis
          atque consequatur nobis pariatur, hic laboriosam minus expedita
          provident dolor repellendus eligendi quisquam dignissimos laborum
          consequuntur corrupti quae? Deserunt, eveniet! Ex.
        </p>
        <p className='bold'>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores esse
          cupiditate pariatur similique minus doloribus inventore nulla nisi, ad
          explicabo laudantium est deserunt eveniet saepe sunt velit, excepturi
          voluptas culpa.
        </p>
        <p className='small'>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem
          quisquam repudiandae, nisi harum aperiam impedit doloremque quibusdam,
          et exercitationem, dolore magni iure explicabo velit incidunt!
          Reiciendis voluptatum tempore eum voluptate?
        </p>
        <p className='small bold'>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem
          quisquam repudiandae, nisi harum aperiam impedit doloremque quibusdam,
          et exercitationem, dolore magni iure explicabo velit incidunt!
          Reiciendis voluptatum tempore eum voluptate?
        </p>
      </section>

      <h2 className='bold'>Inputs</h2>
      <hr className='full-w' />
      <section className='p-ui-kit--section p-ui-kit--section__body p-ui-kit--section__white'>
        <Input label='Name' placeholder='Write the name' />
        <Input label='Name' type='date' />
        <Select label='Select' options={selectOption} />
      </section>

      <h2 className='bold'>Collapse</h2>
      <hr className='full-w' />
      <section className='p-ui-kit--section p-ui-kit--section__body'>
        <Collapse title='Example 1'>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum,
            unde officiis? Mollitia aliquam perferendis ad deserunt accusantium
            molestias alias natus explicabo, ipsum officiis optio molestiae
            dolor obcaecati repudiandae? Vero, illum!
          </p>
        </Collapse>
        <Collapse title='Example 2'>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum,
            unde officiis? Mollitia aliquam perferendis ad deserunt accusantium
            molestias alias natus explicabo, ipsum officiis optio molestiae
            dolor obcaecati repudiandae? Vero, illum!
          </p>
        </Collapse>
        <Collapse title='Example 3'>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum,
            unde officiis? Mollitia aliquam perferendis ad deserunt accusantium
            molestias alias natus explicabo, ipsum officiis optio molestiae
            dolor obcaecati repudiandae? Vero, illum!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum,
            unde officiis? Mollitia aliquam perferendis ad deserunt accusantium
            molestias alias natus explicabo, ipsum officiis optio molestiae
            dolor obcaecati repudiandae? Vero, illum!
          </p>
        </Collapse>
      </section>

      <h2 className='bold'>Modals</h2>
      <hr className='full-w' />
      <section className='p-ui-kit--section p-ui-kit--section__body'>
        <ModalExample />
      </section>

      <h2 className='bold'>LIsto of Icons</h2>
      <hr className='full-w' />
      <section className='p-ui-kit--section p-ui-kit--section__body'>
        <Icons />
      </section>
    </main>
  );
};

export default UIKitPage;
